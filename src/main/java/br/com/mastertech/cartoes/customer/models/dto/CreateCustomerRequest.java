package br.com.mastertech.cartoes.customer.models.dto;

public class CreateCustomerRequest {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
