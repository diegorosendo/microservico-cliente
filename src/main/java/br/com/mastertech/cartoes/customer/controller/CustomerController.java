package br.com.mastertech.cartoes.customer.controller;

import br.com.mastertech.cartoes.customer.models.Customer;
import br.com.mastertech.cartoes.customer.models.dto.CreateCustomerRequest;
import br.com.mastertech.cartoes.customer.models.dto.CustomerMapper;
import br.com.mastertech.cartoes.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class CustomerController {

    @Autowired
    private CustomerService service;

    @Autowired
    private CustomerMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Customer create(@RequestBody CreateCustomerRequest createCustomerRequest) {
        Customer customer = mapper.toCustomer(createCustomerRequest);

        customer = service.create(customer);

        return customer;
    }

    @GetMapping("/{id}")
    public Customer getById(@PathVariable Long id) {

        System.out.println("Chamou ms-cliente");

        return service.getById(id);
    }

}
